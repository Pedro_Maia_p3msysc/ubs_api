package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.DistribuiProdutos;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DistribuiProdutosRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-02-18T14:57:04.305Z[GMT]")
public class DistribuiProdutosRequest   {
  @JsonProperty("distribuiProdutos")
  private DistribuiProdutos distribuiProdutos = null;

  public DistribuiProdutosRequest distribuiProdutos(DistribuiProdutos distribuiProdutos) {
    this.distribuiProdutos = distribuiProdutos;
    return this;
  }

  /**
   * Get distribuiProdutos
   * @return distribuiProdutos
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    @Valid
    public DistribuiProdutos getDistribuiProdutos() {
    return distribuiProdutos;
  }

  public void setDistribuiProdutos(DistribuiProdutos distribuiProdutos) {
    this.distribuiProdutos = distribuiProdutos;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DistribuiProdutosRequest distribuiProdutosRequest = (DistribuiProdutosRequest) o;
    return Objects.equals(this.distribuiProdutos, distribuiProdutosRequest.distribuiProdutos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(distribuiProdutos);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DistribuiProdutosRequest {\n");
    
    sb.append("    distribuiProdutos: ").append(toIndentedString(distribuiProdutos)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
