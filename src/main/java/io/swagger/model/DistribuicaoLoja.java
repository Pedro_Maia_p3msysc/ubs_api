package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.DistribuicaoProdutosLoja;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DistribuicaoLoja
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-02-18T14:57:04.305Z[GMT]")
public class DistribuicaoLoja   {
  @JsonProperty("distribuicaoProdutosLojas")
  @Valid
  private List<DistribuicaoProdutosLoja> distribuicaoProdutosLojas = null;

  @JsonProperty("quantity")
  private Integer quantity = null;

  @JsonProperty("financial")
  private BigDecimal financial = null;

  @JsonProperty("averagePrice")
  private BigDecimal averagePrice = null;

  public DistribuicaoLoja distribuicaoProdutosLojas(List<DistribuicaoProdutosLoja> distribuicaoProdutosLojas) {
    this.distribuicaoProdutosLojas = distribuicaoProdutosLojas;
    return this;
  }

  public DistribuicaoLoja addDistribuicaoProdutosLojasItem(DistribuicaoProdutosLoja distribuicaoProdutosLojasItem) {
    if (this.distribuicaoProdutosLojas == null) {
      this.distribuicaoProdutosLojas = new ArrayList<DistribuicaoProdutosLoja>();
    }
    this.distribuicaoProdutosLojas.add(distribuicaoProdutosLojasItem);
    return this;
  }

  /**
   * Get distribuicaoProdutosLojas
   * @return distribuicaoProdutosLojas
  **/
  @ApiModelProperty(value = "")
      @Valid
    public List<DistribuicaoProdutosLoja> getDistribuicaoProdutosLojas() {
    return distribuicaoProdutosLojas;
  }

  public void setDistribuicaoProdutosLojas(List<DistribuicaoProdutosLoja> distribuicaoProdutosLojas) {
    this.distribuicaoProdutosLojas = distribuicaoProdutosLojas;
  }

  public DistribuicaoLoja quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Get quantity
   * @return quantity
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public DistribuicaoLoja financial(BigDecimal financial) {
    this.financial = financial;
    return this;
  }

  /**
   * Get financial
   * @return financial
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    @Valid
    public BigDecimal getFinancial() {
    return financial;
  }

  public void setFinancial(BigDecimal financial) {
    this.financial = financial;
  }

  public DistribuicaoLoja averagePrice(BigDecimal averagePrice) {
    this.averagePrice = averagePrice;
    return this;
  }

  /**
   * Get averagePrice
   * @return averagePrice
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    @Valid
    public BigDecimal getAveragePrice() {
    return averagePrice;
  }

  public void setAveragePrice(BigDecimal averagePrice) {
    this.averagePrice = averagePrice;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DistribuicaoLoja distribuicaoLoja = (DistribuicaoLoja) o;
    return Objects.equals(this.distribuicaoProdutosLojas, distribuicaoLoja.distribuicaoProdutosLojas) &&
        Objects.equals(this.quantity, distribuicaoLoja.quantity) &&
        Objects.equals(this.financial, distribuicaoLoja.financial) &&
        Objects.equals(this.averagePrice, distribuicaoLoja.averagePrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(distribuicaoProdutosLojas, quantity, financial, averagePrice);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DistribuicaoLoja {\n");
    
    sb.append("    distribuicaoProdutosLojas: ").append(toIndentedString(distribuicaoProdutosLojas)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    financial: ").append(toIndentedString(financial)).append("\n");
    sb.append("    averagePrice: ").append(toIndentedString(averagePrice)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
