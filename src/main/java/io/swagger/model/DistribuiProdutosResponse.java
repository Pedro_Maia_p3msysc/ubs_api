package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.DistribuicaoLoja;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DistribuiProdutosResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-02-18T14:57:04.305Z[GMT]")
public class DistribuiProdutosResponse   {
  @JsonProperty("distribuicaoLojas")
  private DistribuicaoLoja distribuicaoLojas = null;

  public DistribuiProdutosResponse distribuicaoLojas(DistribuicaoLoja distribuicaoLojas) {
    this.distribuicaoLojas = distribuicaoLojas;
    return this;
  }

  /**
   * Get distribuicaoLojas
   * @return distribuicaoLojas
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    @Valid
    public DistribuicaoLoja getDistribuicaoLojas() {
    return distribuicaoLojas;
  }

  public void setDistribuicaoLojas(DistribuicaoLoja distribuicaoLojas) {
    this.distribuicaoLojas = distribuicaoLojas;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DistribuiProdutosResponse distribuiProdutosResponse = (DistribuiProdutosResponse) o;
    return Objects.equals(this.distribuicaoLojas, distribuiProdutosResponse.distribuicaoLojas);
  }

  @Override
  public int hashCode() {
    return Objects.hash(distribuicaoLojas);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DistribuiProdutosResponse {\n");
    
    sb.append("    distribuicaoLojas: ").append(toIndentedString(distribuicaoLojas)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
