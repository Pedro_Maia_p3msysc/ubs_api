package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.DistribuiProdutosResponse1;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DistribuiProdutosResponse1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-02-18T14:57:04.305Z[GMT]")
public class DistribuiProdutosResponse1   {
  @JsonProperty("distribuiProdutosResponse")
  private DistribuiProdutosResponse1 distribuiProdutosResponse = null;

  public DistribuiProdutosResponse1 distribuiProdutosResponse(DistribuiProdutosResponse1 distribuiProdutosResponse) {
    this.distribuiProdutosResponse = distribuiProdutosResponse;
    return this;
  }

  /**
   * Get distribuiProdutosResponse
   * @return distribuiProdutosResponse
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    @Valid
    public DistribuiProdutosResponse1 getDistribuiProdutosResponse() {
    return distribuiProdutosResponse;
  }

  public void setDistribuiProdutosResponse(DistribuiProdutosResponse1 distribuiProdutosResponse) {
    this.distribuiProdutosResponse = distribuiProdutosResponse;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DistribuiProdutosResponse1 distribuiProdutosResponse1 = (DistribuiProdutosResponse1) o;
    return Objects.equals(this.distribuiProdutosResponse, distribuiProdutosResponse1.distribuiProdutosResponse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(distribuiProdutosResponse);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DistribuiProdutosResponse1 {\n");
    
    sb.append("    distribuiProdutosResponse: ").append(toIndentedString(distribuiProdutosResponse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
