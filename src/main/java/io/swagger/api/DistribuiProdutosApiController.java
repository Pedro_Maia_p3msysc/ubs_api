package io.swagger.api;

import io.swagger.model.DistribuiProdutosRequest;
import io.swagger.model.DistribuiProdutosResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-02-18T14:57:04.305Z[GMT]")
@Controller
public class DistribuiProdutosApiController implements DistribuiProdutosApi {

    private static final Logger log = LoggerFactory.getLogger(DistribuiProdutosApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public DistribuiProdutosApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<DistribuiProdutosResponse> distribuiProdutos(@ApiParam(value = "" ,required=true )  @Valid @RequestBody DistribuiProdutosRequest body
) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<DistribuiProdutosResponse>(objectMapper.readValue("{\n  \"distribuicaoLojas\" : {\n    \"quantity\" : 5,\n    \"distribuicaoProdutosLojas\" : [ {\n      \"volume\" : 1.4658129805029452,\n      \"quantity\" : 0,\n      \"price\" : 6.027456183070403\n    }, {\n      \"volume\" : 1.4658129805029452,\n      \"quantity\" : 0,\n      \"price\" : 6.027456183070403\n    } ],\n    \"financial\" : 5.637376656633329,\n    \"averagePrice\" : 2.3021358869347655\n  }\n}", DistribuiProdutosResponse.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<DistribuiProdutosResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<DistribuiProdutosResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

}
